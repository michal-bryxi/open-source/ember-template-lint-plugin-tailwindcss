# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v5.0.1] - 2022-07-11

### Fixed

Issue with `Cannot find module 'ember-template-lint-plugin-tailwindcss' from...` has been fixed.

## [v5.0.0] - 2022-07-08

### Changed

- Swapped Jest for [Vitest](https://vitest.dev/).
- Bumped peerDependecy of `ember-template-lint` to `^4.0.0`

## [v4.0.2] - 2021-12-10

- Bumped version of `ember-template-recast@6.1.2` dependency. Previous version produced invalid template in some complicated cases.

## [v4.0.1] - 2021-12-09

### Fixed

- Removed ember-template-recast version pinning. If you happen to still have issues with the extension putting double quotes into your templates, put following into your `package.json`:

```json
"resolutions": {
  "ember-template-recast": "6.1.1"
}
```

## [4.0.0] - 2021-12-04

### Fixed

- When using linebreak-between-groups mode the plugin should produce better results when class attribute contains mustaches. Previously mustache statements were interleaved with a single space. Now they behave as a separate group each and get their own linebreak & indentation.
- Indentation of mustaches should match whatever was there before.
- A *lot* of test coverage improvements.
- A *lot* of code cleanups.

### Changed

- **BREAKING**: Version of `ember-template-lint` in your `devDependencies` needs to be `>= 3.0.0`
- Improved wording for "concatenated classes" warning
- Bumped _statically_ generated class list to match default config of `tailwindcss@2.2.4`

## [3.2.0] - 2021-04-18

### Fixed

- 🐛 Previously the plugin would try to fix _concatenated dynamic class attributes_ like: `class="bar-{{@baz}} foo` which would result in: `class="bar- foo {{@baz}}"`. Which is probably not what the user wanted. Now this situation is deemed unfixable.

### Added

- 🦶 New anti-footgun settings for _concatenated attribues_ called `warnForConcat` that for backwards compatibility of `2.x` series defaults to `false`. When enabled this settings will mark problematic _concatenated attributes_ like `class="bar-{{@baz}} foo`.

## [3.1.2] - 2021-04-17

### Fixed

- Previously two mustache statements would be meld together without any space between them on fix.
- Previously when reconstructing line broken `class` attribute that contained mustache statements the indentation of closing quote was wrong. For real fixed, totes good, patched for sure.

## [3.1.1] - 2021-04-15

### Fixed

- Previously single mustache statement inside a string would cause the string to be prepended by a single space.
- Previously when reconstructing line broken `class` attribute that contained mustache statements the indentation of closing quote was wrong.

### Changed

- This is not completely new, but for the time being apart from being moved to the end of `class` attribute, mustache statements od _not_ change their horizontal position. They should be preserved exactly at the spot where the author put them, including surrounding whitespaces.

## [3.1.0] - 2021-04-14

### Added

- 🥸 Support for mustaches has landed!
- Mustaches are always last.
- They should be neatly formatted even for cases with linebreaks.
- They should be neatly formatted even for cases with hinted linebreaks.
- For backwards compatibility in `2.x` series they are _disabled_ in the default config via `disableForMustaches: true`. In `5.x` when we know that this is not buggy, we will flip the default settings to `disableForMustaches: false`.

### Changed

- A lot of internal refactorings & code simplification. Might run slightly faster now.

## [3.0.1] - 2021-03-14

### Added

- Better test coverage.

### Changed

- Better internal choice of words.
- Core of the plugin has been converted to TypeScript.
- Performance improvements.

### Fixed

- Thanks to the TypeScript conversion uncovered two edge-case bugs that should be fixed now.
- Make sure that logging message is the original `class` attribute value independently on internal code paths.

## [3.0.0] - 2021-03-01

### Added

- `class-order` rule now has an option to specify `onlyWithHint` string for `linebreakBetweenGroups` option. Which will enable linebreaks _only_ when `class` attribute starts with given _hint_ string.
- `class-order` rule now has option to specify `indent` number for `linebreakBetweenGroups` option. Which will enable customization on the number of spaces prepended before rules on new lines.

### Changed

- **BREAKING**: Thanks to change of `linebreakBetweenGroups` option from simple `true|false` to `false | { onlyWithHint: string, indent: number }`, this change has to be marked as breaking because for users with `linebreakBetweenGroups: true` they would need to change to: `linebreakBetweenGroups: { indent: 7 }`. Users with default settings don't need to do anything. Hopefully not a big impact.

## [2.1.0] - 2021-02-23
### Added

- `linebreakBetweenGroups` option for `class-order` that allows you to have linebreaks that honor the class grouping.
### Changed

- `class-order` does not check whether there is a drift ignoring whitechars.
- Repository of the project changed location, so updated all the internal paths.

### Fixed

- Better test coverage, split tests to smaller units, test examples now use `stripIndent`.
- Fixed a bug where rule `class-order` used to reformat whitespaces around `class` attribute if there were any linebreaks around it.
- Bumped tailwindcss classes to contain all v2.0.0 ones.

## [2.0.0] - 2021-02-06

### Added

- Renovate bot integration.
### Changed

- **BREAKING**: Upgraded all the dependencies.
- **BREAKING**: Rule `class-wrap` is by default disabled to prevent conflicts with `ember-template-lint-plugin-prettier`. Might be reconsidered later once we have a good solution.
- **BREAKING**: Default configuration for `class-order` now utilizes `alphabet` as default. Might be reconsidered later once we have a good solution.
- Updated example in README.

### Fixed

- Rule `class-wrap` now honors default configuration.

## [1.1.0] - 2021-02-06

### Added

- New `sortBy` config option for `order-by` rule that allows to order according to **alphabet** or `sampleClassList`.

## [1.0.0] - 2020-06-27

### Breaking Changes

- One universal linting rule has been split to `class-order` and `class-wrap`. Therefore if you set `classesPerLine` in `class-order`, you now want to move that configuration to `class-wrap`. See [README.md](./README.md) for example.

### Changed

- Since we have now two rules, it's possible to disable each rule independently when necessary.

## [0.3.0] - 2020-06-21

### Added

- New `classesPerLine = integer|null` configuration option that enforces line wrap based on the number of classes per line.

## [0.2.1] - 2020-06-02

### Changed

- Log source contains complete tag to provide more context.

### Fixed

- #5 Cannot read property split of undefined.

## [0.2.0] - 2020-05-16

### Added

- First experimental implementation of `--fix` switch.
- Add GitLab CI integration.
- Enable tests on CI.
- Add internal linting & enable it on CI.

## [0.1.0] - 2020-05-9

### Added

- First experimental implementation of eslint `class-order`.
