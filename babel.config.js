import { createRequire } from "module";

const require = createRequire(import.meta.url);
const resolve = require.resolve;

export default {
  presets: [["@babel/preset-env", { targets: { node: "current" } }]],
  plugins: [
    [
      resolve("@babel/plugin-transform-typescript"),
      {
        allowDeclareFields: true,
        onlyRemoveTypeImports: true,
        // Default enums are IIFEs
        optimizeConstEnums: true,
      },
    ],
  ],
};
