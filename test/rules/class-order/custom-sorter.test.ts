import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {
    sorters: {
      reverse: (a: string, b: string) => {
        return a > b ? -1 : 1;
      },
    },
    groups: [
      {
        matchBy: "tailwindClass",
        sortBy: "alphabet",
        order: 1,
      },
      {
        matchBy: "all",
        sortBy: "reverse",
        order: 2,
      },
    ],
  },

  good: [`<div class="bg-white p-1 rounded-lg foo bar"></div>`],

  bad: [
    {
      template: `<div class="bg-white bar p-1 rounded-lg foo"></div>`,
      fixedTemplate: `<div class="bg-white p-1 rounded-lg foo bar"></div>`,
      result: {
        message:
          "HTML class attribute sorting is: 'bg-white bar p-1 rounded-lg foo', but should be: 'bg-white p-1 rounded-lg foo bar'",
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="bg-white bar p-1 rounded-lg foo"`,
      },
    },
  ],
});
