import { generateRuleTests } from "../test-helpers";
import { stripIndent } from "common-tags";

generateRuleTests({
  name: "class-order",

  config: {
    disableForMustaches: false,
    linebreakBetweenGroups: {
      onlyWithHint: `\n`,
      indent: 2,
    },
  },

  good: [
    `<div class="bg-white p-1 rounded-lg"></div>`,
    stripIndent`
      <div
        class="
          something-else
          grid grid-cols-4
          lg:grid-cols-6 md:grid-cols-5
        "
      ></div>
    `,
    stripIndent`
      <div
        class="
          bazfoo
          gap-4 grid grid-cols-2
          lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-3 xl:grid-cols-6
          {{if
            this.hinted
            'bg-red-100'
            'bg-green-100'
          }}
        "
      ></div>
    `,
  ],

  bad: [
    {
      template: stripIndent`
        <div
          class="
            grid-cols-3 gap-4 grid
              baz
              {{this.yo}}
                lg:grid-cols-6 sm:grid-cols-4
          "
        ></div>
      `,
      fixedTemplate: stripIndent`
        <div
          class="
            baz
            gap-4 grid grid-cols-3
            lg:grid-cols-6 sm:grid-cols-4
            {{this.yo}}
          "
        ></div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: '
              grid-cols-3 gap-4 grid
                baz
                {{this.yo}}
                  lg:grid-cols-6 sm:grid-cols-4
            ', but should be: '
              baz
              gap-4 grid grid-cols-3
              lg:grid-cols-6 sm:grid-cols-4
              {{this.yo}}
            '`,
        line: 2,
        column: 2,
        isFixable: true,
        source:
          "" +
          `class="
    grid-cols-3 gap-4 grid
      baz
      {{this.yo}}
        lg:grid-cols-6 sm:grid-cols-4
  "`,
      },
    },
  ],
});
