import { generateRuleTests } from "../test-helpers";
import { stripIndent } from "common-tags";

// If we're changing the logic in these tests,
// we should bump major version.
// This file ensures that no breaking changes
// happen in minor/patch updates.

generateRuleTests({
  name: "class-order",

  config: {},

  good: [
    ``, // empty template
    `<div class=""></div>`, // empty class attrbute
    `<div class="foo"></div>`, // unknown class
    `<div class="mr-3"></div>`, // one class
    `<div class="md:flex"></div>`, // one variant
    `<div class="bar foo bg-white p-1 rounded-lg lg:block md:flex"></div>`, // unknown classes, tailwind classes, tailwind variants
    `<div class="focus-within bar {{this.yolo}} w-full md:block"></div>`, // mustaches are left alone because of disableForMustaches=true
    stripIndent`
      <button
        {{on "click" @onClick}}
        aria-expanded={{if @isOpen "true" "false"}}
        class="bar focus-within w-full md:block"
        data-test-selector={{this.TEST_SELECTORS.HEADER_BUTTON}}
        type="button"
        disabled={{@isDisabled}}
        ...attributes
      >
      </button>
    `, // complex code example
    `<div class="{{con}}{{cat}}"></div>`, // default config does NOT warn against concat statements
  ],

  bad: [
    {
      template: `<div class="md:flex bg-white foo rounded-lg p-2"></div>`,
      fixedTemplate: `<div class="foo bg-white p-2 rounded-lg md:flex"></div>`,
      result: {
        message:
          "HTML class attribute sorting is: 'md:flex bg-white foo rounded-lg p-2', but should be: 'foo bg-white p-2 rounded-lg md:flex'",
        line: 1,
        column: 5,
        isFixable: true,
        source: `class="md:flex bg-white foo rounded-lg p-2"`,
      },
    },
  ],
});
