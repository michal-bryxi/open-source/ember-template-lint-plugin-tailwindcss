import { generateRuleTests } from "../test-helpers";

generateRuleTests({
  name: "class-order",

  config: {
    disableForMustaches: false,
    warnForConcat: true,
  },

  good: [
    `<div class={{cat}}></div>`, // only mustache statement
    `<div class="{{dog}}"></div>`, // single mustache inside quotes
  ],

  bad: [
    {
      template: `<div class="foo-{{pig}}"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: 'foo-{{pig}}'`,
        line: 1,
        column: 5,
        isFixable: false,
        source: `class="foo-{{pig}}"`,
      },
    },
    {
      template: `<div class="foo {{goat}}-bar"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: 'foo {{goat}}-bar'`,
        line: 1,
        column: 5,
        isFixable: false,
        source: `class="foo {{goat}}-bar"`,
      },
    },
    {
      template: `<div class="{{hamster}}{{snake}}"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: '{{hamster}}{{snake}}'`,
        line: 1,
        column: 5,
        isFixable: false,
        source: `class="{{hamster}}{{snake}}"`,
      },
    },
  ],
});
