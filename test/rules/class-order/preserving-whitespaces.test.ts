import { generateRuleTests } from "../test-helpers";
import { stripIndent } from "common-tags";

generateRuleTests({
  name: "class-order",

  config: {},

  good: [],

  bad: [
    {
      template: stripIndent`
        <button
          type="button"
          class="w-full focus-within"
          ...attributes
        >
        </button>
      `,
      fixedTemplate: stripIndent`
        <button
          type="button"
          class="focus-within w-full"
          ...attributes
        >
        </button>
      `,
      result: {
        message:
          "HTML class attribute sorting is: 'w-full focus-within', but should be: 'focus-within w-full'",
        line: 3,
        column: 2,
        isFixable: true,
        source: stripIndent`
            class="w-full focus-within"
        `,
      },
    },
  ],
});
