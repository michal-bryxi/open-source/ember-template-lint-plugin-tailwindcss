import { describe, it, beforeEach } from "vitest";
import { generateRuleTests as _generateTests } from "ember-template-lint";

import plugin from "../../lib/index.js";

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
export function generateRuleTests(testInfo: any): any {
  return _generateTests({
    groupMethodBefore: beforeEach,
    groupingMethod: describe,
    testMethod: it,
    plugins: [plugin],

    ...testInfo,
  });
}
