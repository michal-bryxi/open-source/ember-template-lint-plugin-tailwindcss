import assert from "assert";
import { describe, it } from "vitest";
import { indentIntermediate } from "../../src/utils/indent-intermediate";

const LINEBREAK_BETWEEN_GROUPS = {
  indent: 2,
  onlyWithHint: <const>false,
};

const LINEBREAK_BETWEEN_GROUPS_HINTED = {
  indent: 2,
  onlyWithHint: `\n`,
};

describe("indentIntermediate", function () {
  it("no mustaches", function () {
    assert.strictEqual(
      indentIntermediate(2, "foo bar baz", LINEBREAK_BETWEEN_GROUPS_HINTED),
      " "
    );
  });

  it("no classes string", function () {
    assert.strictEqual(
      indentIntermediate(2, "", LINEBREAK_BETWEEN_GROUPS_HINTED),
      " "
    );
  });

  it("off", function () {
    assert.strictEqual(indentIntermediate(2, "foo bar baz", false), " ");
  });

  it("hinted off", function () {
    assert.strictEqual(
      indentIntermediate(2, "foo bar baz", LINEBREAK_BETWEEN_GROUPS),
      `\n    `
    );
  });

  it("hinted config on, but without hint", function () {
    assert.strictEqual(
      indentIntermediate(2, "foo bar baz", LINEBREAK_BETWEEN_GROUPS_HINTED),
      " "
    );
  });

  it("hinted on", function () {
    assert.strictEqual(
      indentIntermediate(2, `\nfoo bar baz`, LINEBREAK_BETWEEN_GROUPS_HINTED),
      `\n    `
    );
  });
});
