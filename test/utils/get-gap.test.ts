import assert from "assert";
import { describe, it } from "vitest";
import { getGap } from "../../src/utils/get-gap";

describe("getGap()", function () {
  it("should get correct gap", function () {
    assert.deepStrictEqual(getGap(4), `\n` + " ".repeat(4));
  });
});
