import assert from "assert";
import { describe, it } from "vitest";
import getOriginalClassAttribute from "../../src/utils/get-original-class-attribute";
import { builders } from "ember-template-recast";

describe("getOriginalClassAttribute()", function () {
  it("with strings", function () {
    const input = builders.attr("class", builders.text("with strings"));
    const output = getOriginalClassAttribute(input);
    const expected = "with strings";

    assert.strictEqual(output, expected);
  });

  it("with hbs", function () {
    const input = builders.attr("class", builders.mustache("hbs"));
    const output = getOriginalClassAttribute(input);
    const expected = "{{hbs}}";

    assert.strictEqual(output, expected);
  });

  it("with concats", function () {
    const input = builders.attr(
      "class",
      // eslint-disable-next-line unicorn/prefer-spread
      builders.concat([
        builders.text("with"),
        builders.mustache("some"),
        builders.text("concats"),
      ])
    );
    const output = getOriginalClassAttribute(input);
    const expected = "with{{some}}concats";

    assert.strictEqual(output, expected);
  });
});
