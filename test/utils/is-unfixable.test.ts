import assert from "assert";
import { describe, it } from "vitest";
import isUnfixable from "../../src/utils/is-unfixable";

describe("isUnfixable()", function () {
  it("should say yes", function () {
    assert.deepStrictEqual(isUnfixable("{{foo}}-bar"), true);
    assert.deepStrictEqual(isUnfixable("foo-{{bar}}"), true);
    assert.deepStrictEqual(isUnfixable("foo {{bar}}-baz"), true);
    assert.deepStrictEqual(isUnfixable("foo bar-{{baz}}"), true);
    assert.deepStrictEqual(isUnfixable("foo{{bar}}baz"), true);
    assert.deepStrictEqual(isUnfixable("foo {{bar}}{{baz}}"), true);
    assert.deepStrictEqual(isUnfixable("{{foo}}{{bar}}{{baz}}"), true);
  });

  it("should say no", function () {
    assert.deepStrictEqual(isUnfixable(""), false);
    assert.deepStrictEqual(isUnfixable("   "), false);
    assert.deepStrictEqual(isUnfixable("foo"), false);
    assert.deepStrictEqual(isUnfixable("foo bar"), false);
    assert.deepStrictEqual(isUnfixable("foo {{bar}} baz"), false);
    assert.deepStrictEqual(isUnfixable("{{foo}} bar {{baz}}"), false);
  });
});
