import assert from "assert";
import { describe, it } from "vitest";
import indentLast from "../../src/utils/indent-last";

describe("indentLast()", function () {
  it("should return nothing", function () {
    assert.deepStrictEqual(indentLast("", "", 0), "");
    assert.deepStrictEqual(indentLast("foo bar", "", 0), "");
    assert.deepStrictEqual(indentLast(`\nfoo bar`, "", 0), "");
  });

  it("should return respective amount of spaces", function () {
    assert.deepStrictEqual(indentLast(`\nfoo bar`, `\n`, 4), `\n    `);
  });
});
