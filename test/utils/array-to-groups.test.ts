import { describe, it } from "vitest";
import assert from "assert";
import { Matchers, Sorters } from "../../src/types";
import arrayToGroups from "../../src/utils/array-to-groups";
import { builders } from "ember-template-recast";

const GROUPS = [
  {
    matchBy: "variant",
    sortBy: "alphabet",
    order: 2,
  },
  {
    matchBy: "all",
    sortBy: "alphabet",
    order: 1,
  },
];
const MATCHERS: Matchers = {
  variant: (item) => /:/.test(item),
  all: (item) => /.*/.test(item),
};
const SORTERS: Sorters = {
  alphabet: (a, b) => (a < b ? -1 : 1),
};

describe("arrayToGroups()", function () {
  it("should create groups", function () {
    const input = ["foo", "variant:bar", "bar", "variant:lorem", "baz"];
    const output = arrayToGroups(input, GROUPS, MATCHERS, SORTERS);
    const expected = [
      builders.text("bar baz foo"),
      builders.text("variant:bar variant:lorem"),
    ];

    assert.deepStrictEqual(output, expected);
  });

  it("should handle empty classesArray", function () {
    const input: Array<string> = [];
    const output = arrayToGroups(input, GROUPS, MATCHERS, SORTERS);
    const expected: Array<string> = [];

    assert.deepStrictEqual(output, expected);
  });

  it("should handle empty groups", function () {
    const input: Array<string> = [];
    const output = arrayToGroups(input, [], MATCHERS, SORTERS);
    const expected: Array<string> = [];

    assert.deepStrictEqual(output, expected);
  });

  // TODO: maybe simply append extra groups at the end?
  // it("should figures out what to do if no matcher matches", function () {
  //   const matchers = {
  //     variant: (item) => item.match(/:/),
  //     all: (item) => item.match(/b.*/),
  //   };
  //   const input = ["foo", "variant:bar", "bar", "variant:lorem", "baz"];
  //   const output = arrayToGroups(input, GROUPS, MATCHERS, SORTERS);
  //   const expected = [
  //     ["bar", "baz", "foo"],
  //     ["variant:bar", "variant:lorem"],
  //   ];

  //   assert.deepStrictEqual(output, expected);
  // });
});
