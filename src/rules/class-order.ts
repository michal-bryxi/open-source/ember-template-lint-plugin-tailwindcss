import type { FullClassOrderConfig } from "../types";
import type { AST } from "ember-template-recast";

import { ASTHelpers as helpers } from "ember-template-lint";
import { builders } from "ember-template-recast";
import { Rule } from "ember-template-lint";
import { createErrorMessage } from "../utils/create-error-message";
import { classOrder as DEFAULT_CONFIG } from "./_default-config";
import attributeToArray from "../utils/attribute-to-array";
import arrayToGroups from "../utils/array-to-groups";
import interleave from "../utils/interleave";
import indentLast from "../utils/indent-last";
import isUnfixable from "../utils/is-unfixable";
import { indentIntermediate } from "../utils/indent-intermediate";
import { print, parse } from "ember-template-recast";
import { indentFirst } from "../utils/indent-first";
import getOriginalClassAttribute from "../utils/get-original-class-attribute";

const { text, concat } = builders;

export default class ClassOrder extends Rule {
  parseConfig(config: FullClassOrderConfig): FullClassOrderConfig {
    if (config === true) {
      return DEFAULT_CONFIG;
    }

    if (config && typeof config === "object") {
      return {
        groups: "groups" in config ? config.groups : DEFAULT_CONFIG.groups,
        matchers: Object.assign({}, DEFAULT_CONFIG.matchers, config.matchers),
        sorters: Object.assign({}, DEFAULT_CONFIG.sorters, config.sorters),
        linebreakBetweenGroups: config.linebreakBetweenGroups
          ? config.linebreakBetweenGroups
          : DEFAULT_CONFIG.linebreakBetweenGroups,
        disableForMustaches:
          config.disableForMustaches ?? DEFAULT_CONFIG.disableForMustaches,
        warnForConcat: config.warnForConcat ?? DEFAULT_CONFIG.warnForConcat,
      };
    }
    const errorMessage = createErrorMessage(
      "class-order",
      [
        "Please refer to https://gitlab.com/michal-bryxi/open-source/ember-template-lint-plugin-tailwindcss/-/blob/feat/9-alphabetical-sorting/docs/rule/class-order.md for details",
      ],
      config
    );

    throw new Error(errorMessage);
  }

  getReconstructedClassAttribute(
    reconstructedValue: AST.ConcatStatement
  ): string {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const temporaryNext = parse("") as any;
    temporaryNext.body = [reconstructedValue];
    return print(temporaryNext).slice(1, -1); // TODO: remove the slice()
  }

  exitUnfixable(
    classAttribute: AST.AttrNode,
    originalClassAttributeString: string
  ): boolean {
    if (isUnfixable(originalClassAttributeString)) {
      if (this.config.warnForConcat) {
        this.log({
          message: `Dynamic segments in class names are not allowed: '${originalClassAttributeString}'`,
          node: classAttribute,
          isFixable: false,
        });
      }
      return true;
    }
    return false;
  }

  visitor(): { ElementNode: (node: AST.ElementNode) => void } {
    return {
      ElementNode: (node: AST.ElementNode) => {
        const classAttribute = helpers.findAttribute(node, "class");

        if (!classAttribute) {
          return;
        }

        const originalClassAttributeString =
          getOriginalClassAttribute(classAttribute);

        let classesString = "";
        let mustaches: AST.MustacheStatement[] = [];

        switch (classAttribute.value.type) {
          case "MustacheStatement":
            mustaches = [classAttribute.value];
            break;
          case "ConcatStatement": {
            const parts = classAttribute?.value?.parts ?? [];
            mustaches = parts.filter(
              (part) => part.type === "MustacheStatement"
            ) as AST.MustacheStatement[];

            if (this.config.disableForMustaches && mustaches.length > 0) {
              return;
            }

            if (
              this.exitUnfixable(classAttribute, originalClassAttributeString)
            ) {
              return;
            }

            classesString = parts
              .filter((part): part is AST.TextNode => part.type === "TextNode")
              .map((part) =>
                part.type === "TextNode" ? part.chars : undefined
              )
              .filter(Boolean)
              .join(" ");
            break;
          }
          case "TextNode":
            classesString = classAttribute.value.chars;
            break;
        }

        const classesArray = attributeToArray(classesString);
        const classesGroups = arrayToGroups(
          classesArray,
          this.config.groups,
          this.config.matchers,
          this.config.sorters
        );

        const FIRST_SPACE = text(
          indentFirst(
            classAttribute.loc.start.column,
            classesString,
            this.config.linebreakBetweenGroups
          )
        );
        const INTERMEDIATE_SPACE = text(
          indentIntermediate(
            classAttribute.loc.start.column,
            classesString,
            this.config.linebreakBetweenGroups
          )
        );
        const LAST_SPACE = text(
          indentLast(
            classesString,
            this.config.linebreakBetweenGroups.onlyWithHint,
            classAttribute.loc.start.column
          )
        );
        const reconstructedValue = concat([
          FIRST_SPACE,
          ...interleave([...classesGroups, ...mustaches], INTERMEDIATE_SPACE),
          LAST_SPACE,
        ]);

        if (this.mode === "fix") {
          classAttribute.value = reconstructedValue;
        }

        const reconstructedClassAttributeString =
          this.getReconstructedClassAttribute(reconstructedValue);

        if (
          originalClassAttributeString !== reconstructedClassAttributeString
        ) {
          this.log({
            message: `HTML class attribute sorting is: '${originalClassAttributeString}', but should be: '${reconstructedClassAttributeString}'`,
            node: classAttribute,
            isFixable: true,
          });
        }
      },
    };
  }
}
