import type { OnlyWithHint } from "../types";

export function isHinted(
  classesString: string,
  onlyWithHint: OnlyWithHint
): boolean {
  if (!onlyWithHint) {
    return false;
  }

  return onlyWithHint.length > 0 && classesString.startsWith(onlyWithHint);
}
