export function createErrorMessage(
  ruleName: string,
  lines: string | string[],
  config: JSON | boolean
): string {
  return [
    `The ${ruleName} rule accepts one of the following values.`,
    lines,
    `You specified \`${JSON.stringify(config)}\``,
  ].join("\n");
}
