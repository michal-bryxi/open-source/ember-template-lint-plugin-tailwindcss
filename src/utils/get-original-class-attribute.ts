import type { AST } from "ember-template-recast";
import { print } from "ember-template-recast";

export default function getOriginalClassAttribute(
  classAttribute: AST.AttrNode
): string {
  const classString = print(classAttribute);

  // MustacheStatement: class={{foo}}
  // Else: class="foo {{bar}} baz"
  return classAttribute.value.type === "MustacheStatement"
    ? classString.slice(6)
    : classString.slice(7, -1);
}
