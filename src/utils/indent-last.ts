import { isHinted } from "./is-hinted";
import { getGap } from "./get-gap";
import type { OnlyWithHint } from "../types";

export default function indentLast(
  classesString: string,
  onlyWithHint: OnlyWithHint,
  column: number
): string {
  if (!isHinted(classesString, onlyWithHint)) {
    return "";
  }

  return getGap(column);
}
