import type { LinebreakBetweenGroups } from "../types";
import { isHinted } from "./is-hinted";

export function indentIntermediate(
  classAttributeStartColumn: number,
  classesString: string,
  linebreakBetweenGroups: LinebreakBetweenGroups
): string {
  if (!linebreakBetweenGroups) {
    return " ";
  }

  if (
    isHinted(classesString, linebreakBetweenGroups.onlyWithHint) ||
    !linebreakBetweenGroups.onlyWithHint
  ) {
    return (
      "\n" +
      " ".repeat(linebreakBetweenGroups.indent + classAttributeStartColumn)
    );
  }

  return " ";
}
