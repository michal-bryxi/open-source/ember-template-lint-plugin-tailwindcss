import type {
  LinebreakBetweenGroups,
  ClassesGroups,
  ReconstructArguments,
} from "../types";

import { isHinted } from "../utils/is-hinted";
import { getGap } from "../utils/get-gap";
import { AST } from "ember-template-recast";

export function reconstructClassesString({
  linebreakBetweenGroups,
  classesString,
  classesGroups,
  classAttribute,
}: {
  linebreakBetweenGroups: LinebreakBetweenGroups;
  classesString: string;
  classesGroups: ClassesGroups;
  classAttribute: AST.AttrNode;
}): string {
  const reconstruct = pickFunction({
    linebreakBetweenGroups,
    classesString,
  });
  return reconstruct({
    groups: classesGroups,
    classAttribute: classAttribute,
    // TODO: this is horrible we should have more elegant way to pass arguments to respective functions without this pseudo-casting
    onlyWithHint: linebreakBetweenGroups
      ? linebreakBetweenGroups.onlyWithHint
      : undefined,
    indent: linebreakBetweenGroups ? linebreakBetweenGroups.indent : 0,
  });
}

export function pickFunction({
  linebreakBetweenGroups,
  classesString,
}: {
  linebreakBetweenGroups: LinebreakBetweenGroups;
  classesString: string;
}): (arguments_: ReconstructArguments) => string {
  if (linebreakBetweenGroups === false) {
    return simpleReconstruction;
  }

  if (!linebreakBetweenGroups.onlyWithHint) {
    return linebreakReconstruction;
  }

  if (isHinted(classesString, linebreakBetweenGroups.onlyWithHint)) {
    return hintedLinebreakReconstruction;
  }

  return simpleReconstruction;
}

export function simpleReconstruction({ groups }: ReconstructArguments): string {
  return groups.flat().join(" ");
}

export function linebreakReconstruction({
  groups,
  classAttribute,
  indent,
}: ReconstructArguments): string {
  const gap = getGap(classAttribute.loc.start.column + indent);
  return groups.map((group) => group.join(" ")).join(gap);
}

export function hintedLinebreakReconstruction({
  groups,
  classAttribute,
  indent,
}: ReconstructArguments): string {
  return (
    getGap(classAttribute.loc.start.column + indent) +
    linebreakReconstruction({
      groups,
      classAttribute,
      indent,
    })
    // + getGap(classAttribute.loc.start.column)
  );
}
