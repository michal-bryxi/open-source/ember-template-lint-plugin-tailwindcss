import type { AST } from "ember-template-recast";
import type { Group, Matchers, Sorters } from "../types";
import { builders } from "ember-template-recast";

export default function arrayToGroups(
  classesArray: string[],
  groups: Group[],
  matchers: Matchers,
  sorters: Sorters
): AST.TextNode[] {
  const groupsCloned = groups.map((group) => ({
    sortBy: group.sortBy,
    matchBy: group.matchBy,
    order: group.order,
    classes: [] as string[],
  }));

  const classGroups = classesArray.reduce((groupsAccumulator, currentClass) => {
    const matchingGroup = groupsAccumulator.find(
      // use find to find first matching group according to used matcher
      (group) =>
        matchers[ // find matcher for current group in matchers
          group.matchBy // get name of the matcher for current group
        ](currentClass) // use the matcher for current group on current class to check if that belongs to given group
    );
    if (matchingGroup) {
      matchingGroup["classes"].push(currentClass); // add currentClass to matched group
    }
    return groupsAccumulator; // return current groupsAccumulator
  }, groupsCloned);

  const sortedClasses = classGroups.map(
    // return groups with sorted classes
    (group) => {
      (group["classes"] || []).sort(
        // sort classes of current group using sorter
        sorters[ // find sorter for current group in sorters
          group["sortBy"] // get name os the sorter for current group
        ]
      );
      return group;
    }
  );

  const orderedGroups = sortedClasses.sort((a, b) => a.order - b.order);

  const cleanedUpGroups = orderedGroups
    .map((group) => (group.classes || []).filter(Boolean))
    .filter((group) => group.length > 0); // extract only classes elements & remove empty elements

  const groupsAst = cleanedUpGroups.map((group) =>
    builders.text(group.join(" "))
  );

  return groupsAst;
}
