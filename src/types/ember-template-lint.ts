declare module "ember-template-lint" {
  import { AST } from "ember-template-recast";
  type LogArgument =
    | {
        message: string;
        line: number;
        column: number;
        source: string;
        isFixable: boolean;
      }
    | {
        message: string;
        isFixable: boolean;
        node: AST.Node;
      };

  export class Rule {
    sourceForNode(
      nodeToPrint:
        | AST.ElementNode
        | AST.TextNode
        | AST.MustacheStatement
        | AST.ConcatStatement
    ): string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    config: any;
    mode: "fix";
    log(argument: LogArgument): void;
  }

  export class ASTHelpers {
    static findAttribute(
      node: AST.ElementNode,
      attribute: string
    ): AST.AttrNode;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export function generateRuleTests(testInfo: any): void;
}

declare module "validate-peer-dependencies";
